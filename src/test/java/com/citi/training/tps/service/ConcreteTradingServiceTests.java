package com.citi.training.tps.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.citi.training.tps.model.State;
import com.citi.training.tps.model.Trade;
import com.citi.training.tps.model.Type;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Date;

@SpringBootTest
public class ConcreteTradingServiceTests {

    public final static Logger LOG = LoggerFactory.getLogger(ConcreteTradingServiceTests.class);

    @Autowired
    private TradingService tradingService;

    @Test
    public void test_createTrade_sanity() {
        Trade dummyTrade = new Trade("", Date.from(Instant.now()).toString(), "TEST2", 100, 100.55, Type.BUY, State.CREATED);
        tradingService.create(dummyTrade);
    }

    @Test
    public void test_showTrades() {
        List<Trade> trades = tradingService.findAll();
        LOG.info("-----------------> \n" + trades);
        assert (trades.size() > 0);
    }

    @Test
    public void test_updateTrade(){
        String exampleJSON = "{'id':'5f7732ecc9167d48bc846ec6','stockTicker':'TSLA'','dateCreated':'2020-01-01','stockQuantity':300, 'reqPrice':400, 'type':'BUY', 'state':'REJECTED_TEST'}";
        String id = "5f7732ecc9167d48bc846ec6";
        int stockQuantity = 100;
        Trade trade = new Trade(id, Date.from(Instant.now()).toString(), "", stockQuantity, 100.55, Type.BUY, State.REJECTED);
        Trade ans = tradingService.create( trade);
        assertEquals(stockQuantity, ans.getStockQuantity());

            
    }
}
