package com.citi.training.tps.service;

import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

import com.citi.training.tps.dao.TradingMongoDB;
import com.citi.training.tps.model.State;
import com.citi.training.tps.model.Trade;
import com.citi.training.tps.model.Type;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class TradingServiceTests {

    @Autowired
    private TradingService tradingService;
    @MockBean
    private TradingMongoDB tradingMongoDBMock;

    private Trade dummyTrade = new Trade("", Date.from(Instant.now()).toString(),"",100,100.55,Type.BUY,State.CREATED);


    @Test
    public void test_createTrade_mockSanityCheck(){
        when(tradingMongoDBMock.save(dummyTrade)).thenReturn(dummyTrade);
        Trade ans= tradingService.create(dummyTrade);
    }


    
}
