package com.citi.training.tps.rest;

import com.citi.training.tps.dao.TradingMongoDB;
import com.citi.training.tps.model.State;
import com.citi.training.tps.model.Trade;
import com.citi.training.tps.model.Type;
import com.citi.training.tps.service.TradingService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Instant;
import java.util.Date;

import static org.mockito.Mockito.when;

@SpringBootTest
public class TradeControllerTests {

    @Autowired
    private TradingService tradingService;
    @MockBean
    private TradingMongoDB tradingMongoDBMock;

    private Trade dummyTrade = new Trade("", Date.from(Instant.now()).toString(),"AAPL",100,100.55, Type.BUY, State.CREATED);


    @Test
    public void test_createTrade_mockSanityCheck(){
        when(tradingMongoDBMock.save(dummyTrade)).thenReturn(dummyTrade);
        Trade ans= tradingService.create(dummyTrade);
        assert(dummyTrade.toString().equals(ans.toString()));
    }
    @Test
    public void test_postTrade_mockSanityCheck(){
        when(tradingMongoDBMock.save(dummyTrade)).thenReturn(dummyTrade);
        Trade ans= tradingService.update(dummyTrade.getId(),dummyTrade);
        assert(dummyTrade.toString().equals(ans.toString()));
    }
    @Test
    public void test_putTrade_mockSanityCheck(){
        when(tradingMongoDBMock.save(dummyTrade)).thenReturn(dummyTrade).then((i)->i.getMock());

        Trade ans= tradingService.create(dummyTrade);
        assert(dummyTrade.toString().equals(ans.toString()));
    }
}
