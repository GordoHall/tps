package com.citi.training.tps.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;
import com.fasterxml.jackson.databind.*;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.http.codec.json.Jackson2JsonEncoder;


public class Trade {

	private static final Logger LOG = LoggerFactory.getLogger(Trade.class);

    private String id;
    private String dateCreated;
    private String stockTicker;
    private int stockQuantity;
    private double reqPrice;
    private Type type;
    private State state;

    public Trade() {
	}

    public Trade(String id, String dateCreated, String stockTicker, int stockQuantity, double reqPrice, Type type,
    State state) {
    this.id = id;
    this.dateCreated = dateCreated;
    this.stockTicker = stockTicker;
    this.stockQuantity = stockQuantity;
    this.reqPrice = reqPrice;
    this.type = type;
    this.state = state;
}


    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getStockTicker() {
		return stockTicker;
	}
	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}
	public int getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public double getReqPrice() {
		return reqPrice;
	}
	public void setReqPrice(double reqPrice) {
		this.reqPrice = reqPrice;
	}

	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Trade [dateCreated=" + dateCreated + ", id=" + id + ", reqPrice=" + reqPrice + ", state=" + state
				+ ", stockQuantity=" + stockQuantity + ", stockTicker=" + stockTicker + ", tradeType=" + type
				+ "]";
	}

    
    
}
