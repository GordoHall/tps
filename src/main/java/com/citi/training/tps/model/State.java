package com.citi.training.tps.model;

public enum State {
    CREATED, PENDING, CANCELLED, REJECTED, FILLED, PARTIALLY_FILLED, ERROR
}
