package com.citi.training.tps.rest;

import java.util.List;
import java.util.Optional;

import com.citi.training.tps.model.Trade;
import com.citi.training.tps.service.TradingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/trades")
@CrossOrigin(origins = "*")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    TradingService tradingService;

    @RequestMapping(method=RequestMethod.GET)

    public List<Trade> getAll() {
       return tradingService.findAll();  
    }

    @RequestMapping(path="/{id}",method=RequestMethod.GET)

    public ResponseEntity getOne(@PathVariable String id){
        Optional<Trade> trade = tradingService.get(id);
        return new ResponseEntity<Trade>(trade.get(),HttpStatus.OK);
    }

    @RequestMapping(path="/{id}",method=RequestMethod.PUT)

    public ResponseEntity putOne(@PathVariable String id, @RequestBody Trade trade){
        tradingService.update(id, trade);
        return new ResponseEntity<Trade>(trade,HttpStatus.OK);
    }
    @RequestMapping(path="/",method=RequestMethod.POST)
    public ResponseEntity postOne(@ModelAttribute Trade trade){
        LOG.debug("posting");
        try {
            tradingService.create(trade);
            return new ResponseEntity<Trade>(trade, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<Throwable>(ex.getCause(), HttpStatus.BAD_REQUEST);
        }
    }
    @RequestMapping(path="/{id}",method=RequestMethod.DELETE)
    public ResponseEntity deleteOne(@PathVariable String id){
        Optional<Trade> trade= tradingService.get(id);
        tradingService.delete(trade.get());
        return new ResponseEntity<Trade>(HttpStatus.valueOf(204));
    }



}
